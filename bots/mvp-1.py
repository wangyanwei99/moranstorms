import math
import sys

from mindstorms import Motor, MSHub
from mindstorms.control import wait_for_seconds


def calibrate(steering_motor: Motor) -> None:
    """calibrate the steering to provide a true zero

    Calibration does the following:
    - Run the motor clockwise at full power
    - Run the motor anti-clockwise at full power to relative position X

    At the end of the procedure, the front steering is centered and the
    position is reset such that an absolute position can be run to for
    the actually steering.

    Parameters
    ----------
    steering_motor: mindstorms.Motor
        motor in charge of steering MVP
    """
    steering_motor.set_stall_detection(True)  # enabled by default
    _ = steering_motor.was_stalled()
    # begin procedure
    steering_motor.run_for_rotations(0.5, speed=100)
    if not steering_motor.was_stalled():
        steering_motor.run_for_rotations(0.5, speed=100)
    steering_motor.stop()
    steering_motor.run_for_degrees(-90, speed=35)
    steering_motor.set_degrees_counted(0)  # reset position tracking


# TODO:
# figure out relationship between turning radius and steering amount

hub = MSHub()
steering_motor = Motor("A")
steering_motor.set_default_speed(100)  # positive: left, negative: right
driving_motor = Motor("B")
driving_motor.set_default_speed(-80)  # gearing makes negative run forward

hub.speaker.beep()
wait_for_seconds(0.5)
calibrate(steering_motor)

driving_motor.start()

# figure eight...?
_ = hub.right_button.was_pressed()
while not hub.right_button.was_pressed():
    steering_motor.run_to_position(75, direction="shortest path")
    wait_for_seconds(1)
    steering_motor.run_to_position(360 - 75, direction="shortest path")
    wait_for_seconds(1)
    # there is extra overhead in the loop repeat, which caused the car to
    # move one direction for longer

hub.speaker.beep()
sys.exit(0)
