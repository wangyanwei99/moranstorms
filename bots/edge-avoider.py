import os
import random
import sys

from mindstorms import App, ColorSensor, DistanceSensor, Motor, MotorPair, MSHub
from mindstorms.control import Timer, wait_for_seconds, wait_until
from mindstorms.operator import (
    equal_to,
    greater_than,
    greater_than_or_equal_to,
    less_than,
    less_than_or_equal_to,
    not_equal_to,
)

print(os.uname())
driving_speed = 15

hub = MSHub()
motor_pair = MotorPair("B", "A")
sensor_color = ColorSensor("C")
sensor_distance = DistanceSensor("D")
sensor_distance.light_up_all()
print("robot initialized")

hub.speaker.beep()
_ = hub.right_button.was_pressed()  # clear out any button press

# attempt to drive
motor_pair.start(steering=0, speed=driving_speed)
hub.motion_sensor.reset_yaw_angle()

while not hub.right_button.was_pressed():
    val_reflected = sensor_color.get_reflected_light()
    val_yaw = hub.motion_sensor.get_yaw_angle()
    print("reflected: {}, yaw: {}".format(val_reflected, val_yaw))
    # we likely could do an equivalent with the distance sensor
    if val_reflected < 50:
        hub.speaker.beep()
        motor_pair.move(
            -5, unit="cm", speed=driving_speed * 2
        )  # quickly back up from the edge
        val_distance = random.randint(1, 10) * 5  # generate a random "rotation"
        motor_pair.move_tank(val_distance, unit="cm", left_speed=-50, right_speed=50)
    motor_pair.start(steering=0, speed=driving_speed)  # resume normal operations

motor_pair.stop()
hub.speaker.beep()
sys.exit(1)
