# Moranstorms

Saved code snippets for Lego Mindstorms Robot Inventor (2020).

Note that the base kit comes with the following:

- Hub (includes speaker, light matrix, and accelerometer)
- Motor (4)
- Distance Sensor
- Color Sensor

From the documentation, there is also a Force Sensor (measuring in
Newtons) that is described. This sensor should be available at some
point in the future, but not at this point. Note that the EV3 sensors do
not appear to be compatible due to the difference in connectors.

## Micropython

When debugging and testing new things out, it's important to remember
that Mindstorms uses Micropython on the backend, and this will sometimes
lead to logic and routines from these packages instead of the Mindstorms
ones. Information about Micropython can be found on [their website][1],
which includes the ability to test code online.

Any Micropython files are denoted as `*.mpy`, which are compiled to
perform well in the low-powered environment of a microcontroller or
other embedded system.

## A note on usage and naming

This is my own work, created from inspecting the available resources,
code, and writing my own programs. I have only used information
available through the Mindstorms app and Hub to figure out the code
here, and have created my own supplemental code for Mindstorms. Since my
code is supported by me, not the LEGO Group, I am naming it after
myself. If there are errors, I am the one at fault.

I have also used and expanded this code to support answering Mindstorms
questions on [StackOverflow][2], where my username is `mmoran0032`.
Robotics and microcontrollers are side-projects for me, but I am
interested in adding systems that provide automatic control and learning
into those systems. I am motivated by the SpaceX Dragon automated ACS
(attitude control system) for control and docking, as well as the
systems built to help drive Mars rovers. Eventually, those techniques
will be incorporated here, and I envision this project becoming more of
a repository for driving routines than general API usage, but that is a
long-term project. Considering that my day job is *not* building these
systems, it's a learning project for me.

Like my computers, I have also renamed my Hub to `DAEDELUS-1` after the
mythical greek architect and craftsman. This name appears to be cleared
out whenever the Hub updates and can be changed through the Mindstorms
app. Since this is the first Mindstorms project I've owned, it gets the
`-1` suffix.

## Research

Want to look at file content and such? Start from the following:

```python
# RESEARCH
import os
print(dir(os))

print(os.listdir())
print(os.listdir("mindstorms"))

def display_file(path: str) -> None:
    f = open(path, "r")
    print(f.read())
    f.close()

print("__INIT__.MPY")
display_file("mindstorms/__init__.mpy")
print("MORANSTORMS.PY")
display_file("mindstorms/moranstorms.py")

# check available code
import re
print(dir(re))
```

[1]: https://micropython.org/
[2]: https://stackoverflow.com/questions/tagged/mindstorms
