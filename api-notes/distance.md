# Distance

All have the same RuntimeError when disconnected as other sensors. I'll exclude it from
below to avoid recopying it over and over. Distance sensor has included lights which are
independent of the actual sensor operation itself and are there for a "face" or
personality for any robot. Lights are 0-100 brightness, no colors

## Lights

API:
```
light_up_all(brightness=100)
Lights up all of the lights on the Distance Sensor at the specified brightness.
Parameters
brightness
The specified brightness of all of the lights.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100% ("0" is off, and "100" is full brightness.)
Default : 100
Errors
TypeError
brightness is not a number.

light_up(right_top, left_top, right_bottom, left_bottom)
Sets the brightness of the individual lights on the Distance Sensor.
Parameters
right_top
The brightness of the light that’s above the right part of the Distance Sensor.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100% ("0" is off, and "100" is full brightness.)
Default : 100
left_top
The brightness of the light that’s above the left part of the Distance Sensor.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100% ("0" is off, and "100" is full brightness.)
Default : 100
right_bottom
The brightness of the light that’s below the right part of the Distance Sensor.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100% ("0" is off, and "100" is full brightness.)
Default : 100
left_bottom
The brightness of the light that’s below the left part of the Distance Sensor.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100% ("0" is off, and "100" is full brightness.)
Default : 100
Errors
TypeError
right_top, left_top, right_bottom or left_bottom is not a number.
# same as color sensor, except this doesn't affect anything for the readout. Here to create
# a "face" for something if it doesn't use the Hub as the face
# the "top" is the side away from the notch, such that LEGO is right-side up
```

Raw code:
```
print("TESTING DISTANCE SENSOR LIGHTS...")
hub.right_button.wait_until_pressed()

# "wake up" sensor
for brightness in (0, 25, 50, 75, 100):
    sensor_distance.light_up_all(brightness)
    wait_for_seconds(0.5)

print("wake up complete")
hub.right_button.wait_until_pressed()

# cycle through lights
brightness = [100, 0, 0, 0]

for _ in range(4):
    print(brightness)
    sensor_distance.light_up(*brightness)
    brightness = [brightness[3], brightness[0], brightness[1], brightness[2]]  # rotate list
    wait_for_seconds(0.5)
sensor_distance.light_up_all()

print("blinking complete")

print("DISTANCE SENSOR TEST COMPLETE LIGHTS")
```

## Measurement

Measurement is in distances (e.g. cm) and percentages. It's sonar (I think?) and is affected
by the size, shape, etc. of the object. Smaller objects can only really be detected at close
range reliably it seems...

The Ultrasonic sensor from EV3 (previous Mindstorms set) appears to be the same. It's properties are listed as

- Measures distances between one and 250 cm (one to 100 in.)
- Accurate to +/- 1 cm (+/- .394 in.)

This seemed decently close to what we had in our tests, but possibly with a worse error and smaller range. We would need to do more extensive studies to determine if that's true.

API:
```
get_distance_cm(short_range: bool = False)
Retrieves the measured distance in centimeters.
Parameters
short_range
Whether or not to use short range mode. Short range mode increases accuracy, but it can only detect nearby objects.
Type : boolean
Values : True or False
Default : False
Returns
The measured distance or "none" if the distance can't be measured.
Type : float (decimal number)
Values : 0 to 200 cm
# short range mode will only detect up to ~30-40 cm away
# needs to be facing a flat, relatively large object
# there's also a get_distance_inches() which appears to have the same limits
# max 79" = 200.66 cm
# the cm output was int despite the docstring saying float, and I'd assume the same would be
# true for the inch measurement as well

get_distance_percentage(short_range=False)
Retrieves the measured distance as a percentage.
Parameters
short_range
Whether or not to use short range mode. Short range mode increases accuracy, but it can only detect nearby objects.
Type : boolean
Values : True or False
Default : False
Returns
The measured distance or "none" if the distance can't be measured.
Type : integer (a positive or negative whole number, including 0)
Values : any value between 0 and 100
Errors
TypeError
short_range is not a boolean.
# percentage appears to be roughly the percentage of the maximum range
# for full range, we were around 10% for about 30 cm
# for short range, 80-90% for about 30 cm
# the resolution difference is interesting...short range you get higher resolution measurement
# with percentage, but you get higher resolution for full range with distance. As a note, you
# should never use inches

wait_for_distance_farther_than(distance, unit='cm', short_range=False)
Waits until the measured distance is greater than the specified distance.
Parameters
distance
The target distance to be detected from the sensor to an object.
Type : float (decimal number)
Values : any value
Default : no default value
unit
The unit in which the distance is measured.
Type : String (text)
Values : "cm","in","%"
Default : cm
short_range
Whether or not to use short range mode. Short range mode increases accuracy, but it can only detect nearby objects.
Type : boolean
Values : True or False
Default : False
Errors
TypeError
distance is not a number or unit is not a string or short_range is not a boolean.
ValueError
unit is not one of the allowed values.
# this is blocking, like other `wait_for_*` methods

wait_for_distance_closer_than(distance, unit='cm', short_range=False)
Waits until the measured distance is less than the specified distance.
Parameters
distance
The target distance to be detected from the sensor to an object.
Type : float (decimal number)
Values : any value
Default : no default value
unit
The unit in which the distance is measured.
Type : String (text)
Values : "cm","in","%"
Default : cm
short_range
Whether or not to use short range mode. Short range mode increases accuracy, but it can only detect nearby objects.
Type : boolean
Values : True or False
Default : False
Errors
TypeError
distance is not a number or unit is not a string or short_range is not a boolean.
ValueError
unit is not one of the allowed values. short_range is not a boolean.
# this is blocking
```

Note for below: I'm using my library to support this code.

```python
ms.start_test_suite(hub, "distance sensor")

timer = Timer()
while timer.now() < 5:
    distance = sensor_distance.get_distance_cm()  # 0-200 cm, None if can't be masured
    print(distance)
    wait_for_seconds(0.25)

ms.test_complete(hub, "cm distance")

timer = Timer()  # unless there's a way to reset it...?
while timer.now() < 5:
    distance_sr = sensor_distance.get_distance_cm(short_range=True)  # < 30 cm
    print(distance_sr)
    wait_for_seconds(0.25)

ms.test_complete(hub, "cm short-range distance")

timer = Timer()
while timer.now() < 5:
    distance = sensor_distance.get_distance_percentage()  # 0-100%, None if can't be masured
    print(distance)
    wait_for_seconds(0.25)

ms.test_complete(hub, "percentage distance")

timer = Timer()
while timer.now() < 5:
    distance_sr = sensor_distance.get_distance_percentage(short_range=True)  # < 30 cm
    print(distance_sr)
    wait_for_seconds(0.25)

ms.test_complete(hub, "percentage short-range distance")

sensor_distance.wait_for_distance_farther_than(20, unit="cm", short_range=True)
hub.speaker.beep(60, 0.1)
sensor_distance.wait_for_distance_closer_than(10, unit="cm", short_range=True)

ms.test_complete(hub, "distance wait test")
```
