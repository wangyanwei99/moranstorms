# Light matrix

The Hub has a 5x5 light matrix available, where each pixel can be a different brightness. This
matrix can show letters, numbers, images, or any pixel pattern. There are also built-in animations that
we might be able to access as well.

## API

Note that the light matrix is accessed through `hub.light_matrix`.

```
off()
Turns off all of the pixels on the Light Matrix.

show_image(image: str, brightness: int = 100)
Shows an image on the Light Matrix.
Parameters
image
Name of the image.
Type : String (text)
Values : ANGRY, ARROW_E, ARROW_N, ARROW_NE, ARROW_NW, ARROW_S, ARROW_SE, ARROW_SW, ARROW_W, ASLEEP, BUTTERFLY, CHESSBOARD, CLOCK1, CLOCK10, CLOCK11, CLOCK12, CLOCK2, CLOCK3, CLOCK4, CLOCK5, CLOCK6, CLOCK7, CLOCK8, CLOCK9, CONFUSED, COW, DIAMOND, DIAMOND_SMALL, DUCK, FABULOUS, GHOST, GIRAFFE, GO_RIGHT, GO_LEFT, GO_UP, GO_DOWN, HAPPY, HEART, HEART_SMALL, HOUSE, MEH, MUSIC_CROTCHET, MUSIC_QUAVER, MUSIC_QUAVERS, NO, PACMAN, PITCHFORK, RABBIT, ROLLERSKATE, SAD, SILLY, SKULL, SMILE, SNAKE, SQUARE, SQUARE_SMALL, STICKFIGURE, SURPRISED, SWORD, TARGET, TORTOISE, TRIANGLE, TRIANGLE_LEFT, TSHIRT, UMBRELLA, XMAS, YES
Default : no default value
brightness
Brightness of the image
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100%
Default : 100
Errors
TypeError
image is not a string or brightness is not an integer.
ValueError
image is not one of the allowed values.

set_pixel(x, y, brightness=100)
Sets the brightness of one pixel (one of the 25 LEDs) on the Light Matrix.
Parameters
x
Pixel position, counting from the left.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 4
Default : no default value
y
Pixel position, counting from the top.
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 4
Default : no default value
brightness
Brightness of the pixel
Type : integer (a positive or negative whole number, including 0)
Values : 0 to 100%
Default : 100
Errors
TypeError
x, y or brightness is not an integer.
ValueError
x, y is not within the allowed range of 0-4.

write(text)
Displays text on the Light Matrix, one letter at a time, scrolling from right to left.
Your program will not continue until all of the letters have been shown.
Parameters
text
Text to write.
Type : String (text)
Values : Any text
Default : no default value
# single characters will not scroll, but anything longer will
```

## Code

Below is the testing suite.

```python
ms.start_test_suite(hub, "light matrix")

for idx in range(12):
    hour = idx + 1
    hub.light_matrix.show_image("CLOCK{}".format(hour))
    wait_for_seconds(0.5)
hub.light_matrix.off()

ms.test_complete(hub, "clock")

for _x in range(5):
    for _y in range(5):
        # brightness is 0-100, so let's make them brighter
        brightness = 50 + 3 * (_x + 1) * (_y + 1)
        hub.light_matrix.set_pixel(_x, _y, brightness=brightness)
        wait_for_seconds(0.5)
    wait_for_seconds(1)
hub.light_matrix.off()
# matrix:
#             o
#    0,0 ... 4,0
#    ...     ...
#    0,4 ... 4,4
#       <-O->

for _y in range(5):
    brightness = 20 * (_y + 1)
    for _x in range(5):
        hub.light_matrix.set_pixel(_x, _y, brightness)
# 20% is almost off...we probably want to limit the number of different brightnesses
# we care about for this

ms.test_complete(hub, "pixels")

hub.light_matrix.write("MIKE")
wait_for_seconds(0.5)
hub.light_matrix.write("14")
wait_for_seconds(0.5)
# everything shows a character at a time
hub.light_matrix.write("W")
# single characters will not scroll

ms.test_complete(hub, "writing")
hub.light_matrix.off()
```

## Status light

The light behind the main button can also be adjusted:

MSHub.status_light
The Brick Status Light on the Hub’s Center Button.
Example:

```python
from mindstorms import MSHub

hub = MSHub()

hub.status_light.on('blue')  # likely the same enumerated color list as for the color sensor detector
```

Full API:

```
on(color='white')
Sets the color of the light.
Parameters
color
Illuminates the Hub’s Brick Status Light in the specified color.
Type : String (text)
Values : "azure","black","blue","cyan","green","orange","pink","red","violet","yellow","white"
Default : "white"
Errors
TypeError
color is not a string.
ValueError
color is not one of the allowed values.

off()
Turns off the light.
```
