# Motion Sensor

The Hub has a built-in motion sensor that can measure orientation and motion using an accelerometer and gryoscope (both 3-axis). Like other functions, this provides a coarse and fine version.

```
was_gesture(gesture)
Tests whether a gesture has occurred since the last time was_gesture() was used, or since the beginning of the program (for the first use).
Parameters
gesture
The name of the gesture.
Type : String (text)
Values : "shaken","tapped","doubletapped","falling","none"
Default : no default value
Errors
TypeError
gesture is not a string.
ValueError
gesture is not one of the allowed values.
Returns
True if the gesture has occurred since the last time was_gesture() was called, otherwise false.
Type : Boolean
Values : True or False

get_gesture()
Retrieves the most recently-detected gesture.
Returns
The gesture.
Type : String (text)
Values : 'shaken','tapped','doubletapped','falling'

get_orientation()
Retrieves the Hub's current orientation.
Returns
The Hub’s current orientation.
Type : String (text)
Values : 'front','back','up','down','leftside','rightside'
```

Example:
```python
timer = Timer()

gestures = ["shaken", "tapped", "doubletapped", "falling"]
for _g in gestures:
    print(_g)
    while timer.now() < 5:
        print(hub.motion_sensor.was_gesture(_g))
        wait_for_seconds(1)
    timer.reset()
# seems like you need to be pretty aggressive on the motions...not sure how useful this will be

ms.test_complete(hub, "gesture")
```

## Fine orientation

We can measure the pitch, yaw, and roll of the Hub. These are defined based on the reference axes of the Hub itself.

- **Pitch**: the rotation around the left-right (transverse) axis, or the nose dipping up and down
- **Roll**: the rotation around the front-back (longitudinal) axis, or the wings dipping up and down
- **Yaw**: the rotation around the front-back (vertical) axis, or the nose going side-to-side

The "nose" of the hub is the top where you plug in the cable, opposite the button. Positive pitch is nose up, positive roll is
starboard (side that the right arrow is pointing) going down, and positive yaw ist he nose going to the right/starboard.

```
get_roll_angle()
Retrieves the Hub’s roll angle.
Returns
The roll angle, specified in degrees.
Type : Integer (a positive or negative whole number, including 0)
Values : -180 to 180

get_pitch_angle()
Retrieves the Hub’s pitch angle.
Returns
The pitch angle, specified in degrees.
Type : Integer (a positive or negative whole number, including 0)
Values : -180 to 180

get_yaw_angle()
Retrieves the Hub’s yaw angle.
Returns
The yaw angle, specified in degrees.
Type : Integer (a positive or negative whole number, including 0)
Values : -180 to 180

reset_yaw_angle()
Sets the yaw angle to "0."
```

These values can be monitored directly when the Hub is connected to the app. These three values combined are used to determine the
orientation (see `get_orientation()`), and are (partially) derived from the raw acceleromter and gyroscope values. Given direct access to those values, you can calculate the pitch and roll, but not the yaw. Since that value is in reference to some arbitrary rotation, it is also resetable.

Example:
```python
timer.reset()
while timer.now() < 5:
    print(hub.motion_sensor.get_pitch_angle())

timer.reset()
while timer.now() < 5:
    print(hub.motion_sensor.get_roll_angle())

timer.reset()
while timer.now() < 5:
    print(hub.motion_sensor.get_yaw_angle())

ms.test_complete(hub, "pitch-roll-yaw")
```
