import os
import sys

import protocol
import utime

from mindstorms import App

app = App()

filename = "_api/app.mpy"
with open(filename, "r") as f:
    for line in f:
        print(line)

print("== introspection ==")
print(app.__dict__)
object_methods = [
    _name for _name in dir(app)
    if callable(getattr(app, _name))
]
print(object_methods)
print(dir(app))

app.play_sound("Teleport", 50)

hub.speaker.beep()
sys.exit(0)
