# Mindstorms

Stub Python files so that writing and editing code *not* inside of the
app is a little more useful. At this point, the app is not available on
Linux, so this will be very helpful there.

The structure will follow the same as the default Mindstorms imports:

```python
from mindstorms import MSHub, Motor, MotorPair, ColorSensor, DistanceSensor, App
from mindstorms.control import wait_for_seconds, wait_until, Timer
from mindstorms.operator import (
    greater_than,
    greater_than_or_equal_to,
    less_than,
    less_than_or_equal_to,
    equal_to,
    not_equal_to,
)
# import mindstorms.util as util
```

For the interpreted files, I will be estimating their structure from the
`*.mpy` files found on the Hub. These interpretations will be based on
the readable structure of the file, how the objects and functions are
used elsewhere, and my knowledge of Python. I will potentially be wrong
in many places, but will attempt to map out the structure as best as I
can.

A benefit of this code will be the ability to include docstrings
and usage notes outside of the Mindstorms app, which means that code can
be developed in a regular development environment and with the benefit
of linting, tab completion, and everything else that comes along. The
documentation that I write will be a combination of the available
documentation within the Mindstorms app and my own writing. I make no
claim to the Mindstorms documentation, and any inaccuracies are my own.
I am following the [numpydoc docstring guide][1] for all docstrings.

I am making two choices when I am doing these interpretations, which
serve to make this project look more like a modern Python project.

1. I am including type hints where appropriate
1. I am using `black` and `isort` to organize and format the code

## Files

Directory tree of Mindstorms Robot Inventory, accurate as of Hub OS
2.1.4.13. Since the Mindstorms implementation of `os` does not include
`os.walk`, generating this automatically is a little cumbersome, but can
be done. I have not taken those steps here.

```
util/
  adjust_motor_offset/
    lpf2/
      __init__.mpy
      lpf2_logging.mpy
      lpf2_motor_adjust.mpy
      lpf2_motor_eeprom.mpy
      lpf2_packet.mpy
      lpf2_process.mpy
    AbsoluteMotorAdjust.mpy
  __init__.mpy
  animations.mpy
  color.mpy
  constants.mpy
  error_handler.mpy
  log.mpy
  motor.mpy
  print_override.mpy
  resetter.mpy
  rotation.mpy
  schedule.mpy
  scratch.mpy
  sensors.mpy
  storage.mpy
  time.mpy
projects/
  standalone_/
    __init__.mpy
    animation.mpy
    devices.mpy
    display.mpy
    program.mpy
    row.mpy
    util.mpy
  standalone.mpy
  .slots  # mapping between XXXXX.py and Hub number can be found here
  runtime.log
  reset
  XXXX.py  # random 5-digit number for each user-provided project
runtime/
  extensions/
    __init.mpy
    abstract_extensions.mpy
    linegraphmonitor.mpy
    music.mpy
    sound.mpy
    weather.mpy
  __init__.mpy
  dirty_dict.mpy
  multimotor.mpy
  stack.mpy
  timer.mpy
  virtualmachine.mpy
  vm_store.mpy
system/
  callbacks/
    __init__.mpy
    customcallbacks.mpy
  __init__.mpy
  abstractwrapper.mpy
  display.mpy
  motors.mpy
  motorwrapper.mpy
  move.mpy
  movewrapper.mpy
  sound.mpy
_api/
  __init__.mpy
  app.mpy
  button.mpy
  colorsensor.mpy
  control.mpy
  distancesensor.mpy
  forcesensor.mpy
  large_technic_hub.mpy
  lightmatrix.mpy
  motionsensor.mpy
  motor.mpy
  motorpair.mpy
  operator.mpy
  speaker.mpy
  statuslight.mpy
  util.mpy
commands/
  __init__.mpy
  abstract_handler.mpy
  hub_methods.mpy
  light_methods.mpy
  linegraphmonitor_methods.mpy
  motor_methods.mpy
  move_methods.mpy
  program_methods.mpy
  sound_methods.mpy
  wait_methods.mpy
event_loop/
  __init__.mpy
  event_loop.mpy
mindstorms/
  __init__.mpy
  control.mpy
  operator.mpy
  util.mpy
  runtime.log
  reset
programrunner/
  __init__.mpy
protocol/
  __init__.mpy
  notifications.mpy
  rpc_protocol.mpy
  ujsonrpc.mpy
sounds/  # raw sound files used by the Hub
  menu_click
  menu_fastback
  menu_program_start
  menu_program_stop
  menu_shutdown
  startup
spike/
  __init__.mpy
  control.mpy
  operator.mpy
  util.mpy
ui/
  __init__.mpy
  hubui.mpy
boot.py
hub_runtime.mpy
main.py
version.py
.runtime_hash
extra_files/
.extra_files_hash
runtime.log
```

Code to walk through the tree and show file contents:

```python
import os
import sys


def display_file_contents(filepath: str) -> None:
    """show contents of file, note that `.mpy` will be unintelligible"""
    with open(filepath, "r") as f:
        for i, line in enumerate(f):
            print("{:04d}:  {}".format(i, line))


print(os.uname())

print(os.listdir("."))

# example: manually select a directory
print(os.listdir("_api"))

display_file_contents("_api/button.mpy")

sys.exit()
```

[1]: https://numpydoc.readthedocs.io/en/latest/format.html
